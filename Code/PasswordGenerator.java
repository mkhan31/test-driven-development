import java.lang.Math;
public class PasswordGenerator {
    private int length;
    
    public PasswordGenerator(int size){
        this.length = size;
    }

    public String generateWeakPassword(){
        String lowerCase = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder weak = new StringBuilder(this.length);
        for (int i = 0; i < this.length; i++){
            int position = (int)(lowerCase.length() * Math.random());
            weak.append(lowerCase.charAt(position));
        }
        return weak.toString();
    }

    public String generateStrongPassword(){
        String lowerCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder strong = new StringBuilder(this.length);
        for (int i = 0; i < this.length; i++){
            int position = (int)(lowerCase.length() * Math.random());
            strong.append(lowerCase.charAt(position));
        }
        return strong.toString();
    }
    
    public int getLength(){
        return this.length;
    }
}
