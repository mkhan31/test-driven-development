public class User {
    private String username;
    private String password;
    private UserType type;

    public User (String name, String pass, UserType user){
        this.username = name;
        this.password = pass;
        this.type = user;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    public UserType getUserType(){
        return this.type;
    }
}