import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PasswordTest
{
    @Test
    public void sizeTest()
    {
        PasswordGenerator passLength = new PasswordGenerator(10);
        assertEquals(10, passLength.getLength());
    }

    @Test
    public void weakTest()
    {
        PasswordGenerator passWeak = new PasswordGenerator(15);
        assertEquals(15, passWeak.getLength());
    }

    @Test
    public void strongTest()
    {
        PasswordGenerator passStrong = new PasswordGenerator(13);
        passStrong.generateStrongPassword();
        assertEquals(26, passStrong.getLength());
    }
}